package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"os"
	"path"
	"strings"

	"gitlab.com/mccolljr_/rubigo/rb"
)

// Make implements `rubigo make`, the command which
// generates glue.go, glue.c, Makefile, and extconf.rb
func Make() error {
	rbcfg, err := rb.GetRubyConfig()

	if err != nil {
		return fmt.Errorf("error loading ruby configuration")
	}

	// parse Go files

	fmt.Println("parsing")
	packages, err := parser.ParseDir(token.NewFileSet(),
		args.In, nil, parser.ParseComments)

	if err != nil {
		return fmt.Errorf("error parsing files: %s\n", err)
	}

	pkgMain, ok := packages["main"]

	if !ok {
		return fmt.Errorf("main package required")
	}

	ext := Ext{Name: args.Name, Funcs: nil, Config: rbcfg}

	for _, file := range pkgMain.Files {
		for _, decl := range file.Decls {
			fdef, isFdef := decl.(*ast.FuncDecl)

			// ignore non-func declarations, the main func declaration, and unexported func declarations
			if !isFdef || fdef == nil || fdef.Name.String() == "main" || strings.Index(fdef.Doc.Text(), "export") != 0 {
				continue
			}

			ext.Funcs = append(ext.Funcs, NewFunc(fdef))
		}
	}

	// files we need to create/write
	outputs := []string{"glue.h", "glue.go", "Makefile", "extconf.rb"}

	var ofile *os.File
	for _, oname := range outputs {
		fmt.Println("creating", oname)
		ofile, err = os.Create(path.Join(args.Out, oname))

		if err != nil {
			return fmt.Errorf("can't create %s", oname)
		}

		fmt.Println("writing", oname)
		err = Tpls.ExecuteTemplate(ofile, oname, ext)
		if err != nil {
			return fmt.Errorf("can't write %s", oname)
		}
	}

	return nil
}

// RubyInfo implements `rubigo rbinfo`, the command which writes
// a prettified JSON string to STDOUT with your current ruby's
// configuration.
func RubyInfo() error {
	rbcfg, err := rb.GetRubyConfigJSON()

	if err != nil {
		return fmt.Errorf("error loading ruby configuration")
	}

	fmt.Printf("%s\n", rbcfg)

	return nil
}
