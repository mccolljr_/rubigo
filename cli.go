package main

import (
	"fmt"
	"os"
	"path"

	arg "github.com/alexflint/go-arg"
)

// Args holds information for our possible command line arguments
type Args struct {
	Command string `arg:"positional,help:info or make"`
	Name    string `arg:"-n,help:name of the ruby extension (required for make)"`
	In      string `arg:"-i,help:directory to examine go files in"`
	Out     string `arg:"-o,help:directory to write output into"`
}

// Version returns the current version string
func (Args) Version() string {
	return "0.1 alpha"
}

var args Args

func main() {
	// in defaults to the current directory
	args.In = "."
	p := arg.MustParse(&args)

	// do command, or fail if not exists
	switch args.Command {
	case "make":
		if args.Name == "" {
			fmt.Println("name (--name or -n) is required for make")
			os.Exit(1)
		}

		// modify args.In to point to the ext folder of the gem
		// TODO: some kind of logic to determine if we do this
		args.In = path.Join(args.In, "ext", args.Name)

		if args.In != "" && args.Out == "" {
			args.Out = args.In
		}

		err := Make()

		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	case "rbinfo":
		err := RubyInfo()

		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	default:
		fmt.Println("unrecognized command:", args.Command)
		p.WriteHelp(os.Stdout)
		os.Exit(1)
	}
}
