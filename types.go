package main

import (
	"fmt"
	"go/ast"

	"gitlab.com/mccolljr_/rubigo/rb"
)

/* types.go

types.go contains definitions for the types used internally by the CLI to parse
your Go code and generate the necessary files to build a C extension, as well
as any methods on those types.

*/

// Ext contains information about the ruby extension rubigo is
// generating support files for.
type Ext struct {
	Name   string
	Funcs  []*Func
	Config *rb.RubyConfig
}

// Formatter provides an interface
// representing any object that can write itself
// in valid Go syntax and in valid C syntax
type Formatter interface {
	GoFmt() string
	CFmt() string
}

// Type is a specific interface that is simply a wrapper
// for the Formatter interface
type Type interface {
	Formatter
}

// PointerType is a Formatter for a pointer to another type
type PointerType struct {
	Type Type
}

// GoFmt implements Formatter for PointerType
func (pt PointerType) GoFmt() string {
	return "*" + pt.Type.GoFmt()
}

// CFmt implements Formatter for PointerType
func (pt PointerType) CFmt() string {
	return pt.Type.CFmt() + "*"
}

// TypeName is a Formatter for a named type
type TypeName struct {
	C    bool
	Name string
}

// GoFmt implements Formatter for TypeName
func (tn TypeName) GoFmt() string {
	if tn.C {
		return "C." + tn.Name
	}

	return tn.Name
}

// CFmt implements Formatter for TypeName
func (tn TypeName) CFmt() string {
	return tn.Name
}

// Arg is a Formatter representing a function argument
type Arg struct {
	Name string
	Type Type
}

// GoFmt implements Formatter for Arg
func (a Arg) GoFmt() string {
	return a.Name + " " + a.Type.GoFmt()
}

// CFmt implements Formatter for Arg
func (a Arg) CFmt() string {
	return a.Type.CFmt() + " " + a.Name
}

// ArgList is a Formatter that handles writing a list of
// function parameters
type ArgList struct {
	Args []Arg
}

// GoFmt implements Formatter for ArgList
func (al ArgList) GoFmt() string {
	result := ""

	ct := len(al.Args)

	for i := 0; i < ct; i++ {
		result += al.Args[i].GoFmt()

		if i < ct-1 {
			result += ", "
		}
	}

	return result
}

// CFmt implements Formatter for ArgList
func (al ArgList) CFmt() string {
	result := ""

	ct := len(al.Args)

	for i := 0; i < ct; i++ {
		result += al.Args[i].CFmt()

		if i < ct-1 {
			result += ", "
		}
	}

	return result
}

// Returns is a Formatter for the return type(s)
// of a function
type Returns struct {
	Types []Type
}

// GoFmt implements Fomatter for Returns
func (r Returns) GoFmt() string {
	sz := len(r.Types)

	if sz == 0 {
		return ""
	}

	result := "("

	for i := 0; i < sz; i++ {
		result += r.Types[i].GoFmt()

		if i < sz-1 {
			result += ", "
		}
	}

	result += ")"

	return result
}

// CFmt implements Fomatter for Returns
func (r Returns) CFmt() string {
	if r.Types == nil {
		return "void"
	}

	sz := len(r.Types)

	if sz == 0 {
		return "void"
	}

	if sz > 1 {
		panic("return: can't have multiple returns in C")
	}

	return r.Types[0].CFmt()
}

// Func is NOT a Formatter. It wraps a function
// definition, and can write a valid method stub in C
// and Go
type Func struct {
	Name    string
	Takes   ArgList
	Argc    int
	Returns Returns
}

// NewFunc returns a pointer to a Func{} with initial values
func NewFunc(fdef *ast.FuncDecl) *Func {
	// how many arguments does this bad boy take?
	argc := fdef.Type.Params.NumFields()

	// create a new Func object
	fn := &Func{
		Name: fdef.Name.String(),
		Takes: ArgList{
			make([]Arg, argc),
		},
		Argc: argc,
		Returns: Returns{
			nil,
		},
	}

	// get all the arguments
	i := 0
	for _, plist := range fdef.Type.Params.List {

		t := typeFrom(plist.Type)

		for _, an := range plist.Names {
			fn.Takes.Args[i] = Arg{
				Name: an.String(),
				Type: t,
			}

			i++
		}
	}

	if fdef.Type.Results != nil && fdef.Type.Results.List != nil {
		retc := fdef.Type.Results.NumFields()
		fn.Returns.Types = make([]Type, retc)
		j := 0
		for _, rlist := range fdef.Type.Results.List {
			t := typeFrom(rlist.Type)

			if rlist.Names != nil {
				for range rlist.Names {
					fn.Returns.Types[j] = t
					j++
				}
			} else {
				fn.Returns.Types[j] = t
				j++
			}
		}
	}

	return fn
}

// CStub writes f as a valid C method stub
func (f *Func) CStub() string {
	return fmt.Sprintf("%s %s(%s);", f.Returns.CFmt(), f.Name, f.Takes.CFmt())
}

// GoStub writes f as a valid Go method stub
func (f *Func) GoStub() string {
	return fmt.Sprintf("func %s(%s) %s;", f.Name, f.Takes.GoFmt(), f.Returns.GoFmt())
}

// typeFrom takes an Expr from the ast package
// and creates a Type Formatter from it, if possible.
// If it isn't possible, typeFrom panics.
func typeFrom(e ast.Expr) Type {
	switch e.(type) {
	case *ast.SelectorExpr:
		return typeFromSelectorExpr(e.(*ast.SelectorExpr))
	case *ast.StarExpr:
		return typeFromStarExpr(e.(*ast.StarExpr))
	case *ast.Ident:
		return typeFromIdent(e.(*ast.Ident))
	default:
		panic(1)
	}
}

// typeFromSelectorExpr returns a Type from a supported
// ast.SelectorExpr (of the form <X>.<Name>), otherwise
// it panics.
func typeFromSelectorExpr(e *ast.SelectorExpr) Type {
	t := TypeName{Name: e.Sel.String()}

	par, ok := e.X.(*ast.Ident)
	if !ok {
		panic(2)
	}

	if par.String() == "C" {
		t.C = true
	}

	return t
}

// typeFromStarExpr returns a Type from the given
// ast.StarExpr, which represents a pointer expression
// in Go, and based on how rubigo parses a Go file,
// should only ever be a pointer type
func typeFromStarExpr(e *ast.StarExpr) Type {
	return PointerType{
		Type: typeFrom(e.X),
	}
}

// typeFromIdent simply converts the ast.Ident
// passed to it to a TypeName, and returns it as
// a Type
func typeFromIdent(i *ast.Ident) Type {
	return TypeName{C: false, Name: i.String()}
}
