package rb

// TODO: gather CFLAGS from environment
// TODO: implement functionality from https://silverhammermba.github.io/emberb/c/

/*
#cgo CFLAGS: -I /usr/local/Cellar/ruby/2.3.3/include/ruby-2.3.0
#cgo CFLAGS: -I /usr/local/Cellar/ruby/2.3.3/include/ruby-2.3.0/ruby
#cgo CFLAGS: -I /usr/local/Cellar/ruby/2.3.3/include/ruby-2.3.0/x86_64-darwin15
#cgo LDFLAGS: -L "/usr/local/lib" -lruby
#include "ruby.h"
*/
import "C"
import "unsafe"

/*
   Type Conversions
*/

func IntPtr(i *int) *C.int {
	return (*C.int)(unsafe.Pointer(i))
}

/*
   Wrapped Funcs
*/
func EvalString(code string) C.VALUE {
	return C.rb_eval_string(C.CString(code))
}

func EvalStringProtect(code string, state *int) C.VALUE {
	return C.rb_eval_string_protect(C.CString(code), IntPtr(state))
}

func CallFunction(receiver C.VALUE, name string, argc int, args *C.VALUE) C.VALUE {
	return C.rb_funcallv(receiver, C.rb_intern(C.CString(name)), C.int(argc), args)
}
