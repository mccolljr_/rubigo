package main

import "text/template"

const masterTpl = `` +
	// glue_prefix
	// the 'auto-generated' comment for go & c files
	`
{{define "glue_prefix" -}}
// ===========================================
//
// DO NOT EDIT - this file was auto-generated
//
// ===========================================
{{- end}}
` +
	// glue.h
	// generates glue.h with stubs for the exported
	// Go function signatures
	`
{{- define "glue.h" -}}
{{ template "glue_prefix" }}
#include "ruby.h"
/* these stubs are necessary glue to allow Go to build a ruby extension */
{{- range $fn := .Funcs }}
extern {{ $fn.CStub -}}
{{- end }}
{{- end}}
` +
	// glue.go
	// generates glue.go with the necessary #cgo directives
	// to compile a ruby extension
	`
{{- define "glue.go" -}}
{{ template "glue_prefix" }}
package main

/*
#cgo CFLAGS: -I {{.Config.RubyHdrDir}}
#cgo CFLAGS: -I {{.Config.RubyHdrDir}}/ruby
#cgo CFLAGS: -I {{.Config.RubyArchHdrDir}}
#cgo LDFLAGS: {{.Config.LibRubyArg}}
*/
import "C"
{{- end -}}
` +
	// Makefile
	// generates a simple Makefile that gem can use to
	// build the native part of your ruby extension
	`
{{- define "Makefile" -}}
SRCDIR = ext/{{.Name}}
OUTDIR = lib/{{.Name}}
OUTNAME= $(OUTDIR)/{{.Name}}.so

install:
	go build -buildmode=c-shared -o $(OUTNAME)

clean:
    rm $(SRCDIR)/{{.Name}}.h
{{- end -}}
` +
	// extconf.rb
	// creates a stub extconf.rb
	`
{{- define "extconf.rb" -}}
#TODO
"""
This file needs to do the following:

    1. Check for the existence of Go
    2. Check for the existence of Rubigo (? - do we, or should it do what Rubigo does)
    3. Check other deps (? - how do these get determined?)
"""

puts "currently does nothing"
{{- end -}}
`

// Tpls holds all of the templates defined above
var Tpls = template.Must(template.New("").Parse(masterTpl))
