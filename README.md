# Rubigo #

Rubigo is a framework for building Ruby extensions in Go.
It contains 2 components:

`rubigo`: the CLI
1. `make`: parse your Go files, generate glue files, and create a Makefile for use with `gem`
2. `rbinfo`: get JSON representing your current ruby's environment

`rubigo/rb`: a package containing a Go interface to the Ruby C API. This is currently under active development.

This package is not yet stable.
